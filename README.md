package com.company;

public class Main {

    public static void main(String[] args) {
        String s = "Hello World!";
        char[] array;
        array = s.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] != ' ') {
                int count = 1;
                    while (i < array.length - 1 && array[i] == array[i + 1]){
                        count += 1;
                        i++;
                    }
                System.out.print("[" + count + ", "  + "'" + array[i] + "']");
                    if (i < array.length - 1)
                        System.out.print(", ");
            }
        }
    }
}